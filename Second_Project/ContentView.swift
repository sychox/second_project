//
//  ContentView.swift
//  Second_Project
//
//  Created by Bakr Altalhi on 14/08/2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
