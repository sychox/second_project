//
//  Second_ProjectApp.swift
//  Second_Project
//
//  Created by Bakr Altalhi on 14/08/2021.
//

import SwiftUI

@main
struct Second_ProjectApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
